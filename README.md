How to

1. kubectl apply -f NSprod.yml
Create PROD NS

2. kubectl apply -f DP.yml -n prod
Create PROD deployment

3. kubectl apply -f Service.yml -n prod
Create PROD service

4. kubectl apply -f Ingress.yml -n prod
Create PROD Ingress

5. kubectl apply -f NScanary.yml
Create CANARY NS

6. kubectl apply -f DPcanary.yml -n canary
Create CANARY deployment

7. kubectl apply -f Service.yml -n canary
Create CANARY service

8. kubectl apply -f IngressCanary.yml -n canary
Create CANARY Ingress
